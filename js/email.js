﻿var Message = '';

jQuery(document).ready(function () {


    $("#idbuildteamemail").click(function (event)
	{

		event.preventDefault();


        Message = "";


		var emailaddress = $('#idemailbuild').val();


        if (emailaddress == '') {

            Message = 'Missing email address.';

            $("#message5").html(Message);

            return false;

        }

        if (emailaddress != '' && !validateEmail(emailaddress)) {

            Message = 'Email address in wrong format.';

            $("#message5").html(Message);

            return false;
        }


	    var whomanages = $("input[name='whomanages']:checked").val();

		var teammodel = $("input[name='teammodel']:checked").val();

		var teamlocation = $("input[name= 'teamlocation']:checked").val();

	    var requirements = $("input[name= 'requirements']:checked").val();


		var action = "Manages: " + whomanages + " Model: " + teammodel + " Location: " + teamlocation + " Requirements: " + requirements;


        var request = $.ajax({
            //url: "http://localhost:53956/api/email/sendemail",
            url: "https://www.streetgravity.com/webapibuyersinbox/api/email/sendemail",
            type: "GET",
            data: { emailaddress: emailaddress, description: action, emailtype: 'Street Gravity Build' },
            dataType: "json",
            success: function (resp) {


                $("#message5").html("Thank You");

				$("input:radio").attr("checked", false);

				$("#idemailbuild").val("");




            }

        });



        return false;


    });

		

	$("#idclose").click(function (event) {


		$("#idbuildoptions").css("display", "none");

		$('input[type="radio"]').prop('checked', false);


		$("#idemailbuild").val("");


	});



	$("#buildteamform").click(function (event) {


   $("#idbuildoptions").css("display", "block");


	});



	
	$("#idFreeConsultation").click(function (event) {

		event.preventDefault();


		Message = "";


		var emailaddress = $('#work_contact_email').val();

		var comment = $('#work_contact_message').val();




		if (emailaddress == '') {

			Message = 'Missing email address.';

			$("#message1").html(Message);

			return false;

		}

		if (emailaddress != '' && !validateEmail(emailaddress)) {

			Message = 'Email address in wrong format.';

			$("#message1").html(Message);

			return false;
		}

		if (comment == '') {

			Message = ' Missing comment.';

			$("#message1").html(Message);

			return false;
		}




		var request = $.ajax({
			//url: "http://localhost:53956/api/email/sendemail",
			url: "https://www.streetgravity.com/webapibuyersinbox/api/email/sendemail",
			type: "GET",
			data: { emailaddress: emailaddress, description: comment, emailtype: 'Street Gravity Consultation' },
			dataType: "json",
			success: function (resp) {


				$("#message1").html("Thank You");

				$('#work_contact_email').val('');
				$('#work_contact_message').val('');



			}

		});



		return false;


	});






    $("#idStartupIdea").click(function ()
    {


        var emailaddress = $('#contact_email_startup').val();
        var comment = $('#contact_message_startup').val();


        Message = "";



        if (emailaddress == '') {

            Message = 'Missing email address.';

            $("#message2").html(Message);

            return false;

        }

        if (emailaddress != '' && !validateEmail(emailaddress)) {

            Message = 'Email address in wrong format.';

            $("#message2").html(Message);

            return false;
        }

        if (comment == '') {

            Message = ' Missing comment.';

            $("#message2").html(Message);

            return false;
        }





        var request = $.ajax({
            //url: "http://localhost:53956/api/email/sendemail",
			url: "https://www.streetgravity.com/webapibuyersinbox/api/email/sendemail",
            type: "GET",
			data: { emailaddress: emailaddress, description: comment, emailtype: 'Street Gravity Startup' },
            dataType: "json",
            success: function (resp) {


                $("#message2").html("Thank You");

				var emailaddress = $('#contact_email_startup').val('');
				var comment = $('#contact_message_startup').val('');


            }

        });




        return false;



	});




	$("#idPatent").click(function () {


		var emailaddress = $('#contact_email_patent').val();
		var comment = $('#contact_message_patent').val();


		Message = "";



		if (emailaddress == '') {

			Message = 'Missing email address.';

			$("#message3").html(Message);

			return false;

		}

		if (emailaddress != '' && !validateEmail(emailaddress)) {

			Message = 'Email address in wrong format.';

			$("#message3").html(Message);

			return false;
		}

		if (comment == '') {

			Message = ' Missing comment.';

			$("#message3").html(Message);

			return false;
		}





		var request = $.ajax({
			//url: "http://localhost:53956/api/email/sendemail",
			url: "https://www.streetgravity.com/webapibuyersinbox/api/email/sendemail",
			type: "GET",
			data: { emailaddress: emailaddress, description: comment, emailtype: 'Patent Consultation' },
			dataType: "json",
			success: function (resp) {


				$("#message3").html("Thank You");

				var emailaddress = $('#contact_email_patent').val('');
				var comment = $('#contact_message_patent').val('');

			}

		});




		return false;



	});




    $("#idContactSend").click(function () {


        var fullname = $('#contact_fullname').val();
        var subject = $('#contact_subject').val();
        var emailaddress = $('#contact_email').val();
        var comment = $('#contact_message').val();


        Message = "";



        if (emailaddress == '') {

            Message = 'Missing email address.';

            $("#message3").html(Message);

            return false;

        }

        if (emailaddress != '' && !validateEmail(emailaddress)) {

            Message = 'Email address in wrong format.';

            $("#message3").html(Message);

            return false;


        }

        if (comment == '') {

            Message = ' Missing comment.';

            $("#message3").html(Message);

            return false;

        }


        var request = $.ajax({
            //url: "http://localhost:53956/api/email/sendemail",
			url: "https://www.streetgravity.com/webapibuyersinbox/api/email/sendemail",
            type: "GET",
			data: { emailaddress: emailaddress, description: fullname + ' ' + subject + ' ' + comment, emailtype: 'Street Gravity Contact' },
            dataType: "json",
            success: function (resp) {


                $("#message3").html("Thank You");



                $('#contact_fullname').val('');
                $('#contact_subject').val('');
                $('#emailaddress').val('');
                $('#contact_email').val('');

            }

        });




        return false;



    }); 




});






function validateEmail(email) {
    var re = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i

    return re.test(email);
}

var qs = (function (a) {
    if (a == "") return {};
    var b = {};
    for (var i = 0; i < a.length; ++i) {
        var p = a[i].split('=', 2);
        if (p.length == 1)
            b[p[0]] = "";
        else
            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
    }
    return b;
})(window.location.search.substr(1).split('&'));

function hasValue(v) {

    if (v != '' && v != null && v != undefined) {
        return true;
    }
    else {
        return false;
    }
}


